name := "bike-trip-data"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies ++=Seq(
    "org.apache.spark" %% "spark-core" % "2.2.0",
   "org.apache.spark" %% "spark-sql" % "2.2.0"
//   "org.apache.hbase" % "hbase-server" % "1.2.1",
//    "org.apache.hbase" % "hbase-client" % "1.2.1",
//   "org.apache.hbase" % "hbase-common" % "1.2.1",
//   "org.apache.hadoop" % "hadoop-common" % "2.7.3",
//    "org.apache.hbase" % "hbase-spark" % "2.0.0-SNAPSHOT"
)