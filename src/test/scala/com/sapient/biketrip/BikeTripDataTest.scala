package com.sapient.biketrip

import com.sapient.biketrip.BikeTripData.spark

import org.scalatest.FunSpec

class BikeTripDataTest extends FunSpec with SparkSessionTestWrapper
   {
      import spark.implicits._

      val dsStationData = sparkSession.read
        .format("csv")
        .option("header", "true")
        .option("mode", "DROPMALFORMED")
        .load("src/test/resources/station_data.csv")
        .as[StationData]

      assert(dsStationData.count() ==3)




}
