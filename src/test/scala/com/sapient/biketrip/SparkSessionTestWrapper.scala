package com.sapient.biketrip

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val sparkSession: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark test example")
      .getOrCreate()
  }

}
