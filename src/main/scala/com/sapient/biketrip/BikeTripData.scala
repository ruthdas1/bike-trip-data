package com.sapient.biketrip

import org.apache.spark.sql.types.IntegerType

object BikeTripData extends App {
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local")
      .appName("Spark CSV Reader")
      .getOrCreate;

    import spark.implicits._

    val dsStationData = spark.read
      .format("csv")
      .option("header", "true")
      .option("mode", "DROPMALFORMED")
      .load("src/main/resources/station_data.csv")
      .select("station_id", "name","landmark")
      .as[StationData]

    val dsTripData = spark.read
      .format("csv")
      .option("header", "true")
      .option("mode", "DROPMALFORMED")
      .load("src/main/resources/trip_data.csv")
      .select("duration", "startTerminal","endTerminal","bikeNo")
      .withColumn("duration", 'duration.cast(IntegerType))
      .as[TripData]

//    dsTripData
//      .select("bikeNo","startTerminal","endTerminal","duration")
//      .groupBy("bikeNo")
//      .agg(collect_list(struct($"startTerminal",$"endTerminal")).as("value"))
      //.show(15, false)

    val dsTripDataStart = dsTripData
      .join(dsStationData, dsStationData("station_id")===dsTripData("startTerminal"))
      .select("bikeNo","duration","startTerminal","name","landmark")
      //.show(10, false)
      //.printSchema()


    val dsTripDataEnd = dsTripData
      .join(dsStationData, dsStationData("station_id")===dsTripData("endTerminal"))
      .select("bikeNo","duration","endTerminal","name",  "landmark")
      //.show(10, false)
      //.printSchema()


    val bikeRecords = dsTripDataStart
      .join(dsTripDataEnd, "bikeNo")
      .select(dsTripDataEnd("bikeNo") as "bike",
              dsTripDataStart("name") as "start_station",
              dsTripDataEnd("name") as "end_station",
              dsTripDataStart("landmark") as "landmarks")
//      .groupBy("bikeNo")
//      .agg((collect_list(dsTripDataEnd.col("bikeNo")) as "bike"),
//        (collect_list(dsTripDataStart.col("name")) as "start_station"),
//        (collect_list(dsTripDataStart.col("landmark")) as "landmarks"))
      //.show(15, false)

      .toDF()
    SaveRecordJson.writeToJson(bikeRecords, "src/main/resources/output")

}
