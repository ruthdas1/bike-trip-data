package com.sapient.biketrip

import org.apache.spark.sql.{DataFrame, SaveMode}

object SaveRecordJson {

  def writeToJson(input:DataFrame, path:String): Unit ={

    input.write.format("json").mode(SaveMode.Append).save(path)

    /*
    //For partitioned output
    input.write.partitionBy("bike").format("json").mode(SaveMode.Append).save(path)
     */

  }

}
