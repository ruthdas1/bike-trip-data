package com.sapient.biketrip

import org.apache.spark.sql.DataFrame

object DatabasePublisher {

  def catalog = s"""{
                   |"table":{"namespace":"default", "name":"ToolLogs"},
                   |"rowkey":"key",
                   |"columns":{
                   |"timestamp":{"cf":"rowkey", "col":"key", "type":"long"},
                   |"temp":{"cf":"msmt", "col":"temp", "type":"float"},
                   |"pressure":{"cf":"msmt", "col":"pressure", "type":"float"}
                   |}
                   |}""".stripMargin

  def saveToHBase(dfBikeRecord:DataFrame): Unit ={
//    dfBikeRecord.write
//      .options(Map(HBaseTableCatalog.tableCatalog -> catalog, HBaseTableCatalog.newTable -> "5"))
//    .format("org.apache.spark.sql.execution.datasources.hbase")
//    .save()

  }

}
