package com.sapient.biketrip

final case class StationData(
                        station_id:String,
                        name:String,
                        landmark:String)
