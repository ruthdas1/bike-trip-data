Steps to run the application:

1.run com.sapient.biketrip.BikeTripData

2.the output gets appended to path src/resources/output

input files used are station_data and trip_data(few records from the original file)

Test Case:

BikeTripDataTest

=====================================================================

About: This is a standalone spark application, that reads two csv files provided under src/resources and generates output in json format.
The output could be partitioned against "bike" (uncomment the line for partitioned output)

Unable to get the Hbase persistence done due to environment issues and limited time.(output json file to be inserted into NoSQL Hbase)
Also as the virtual box was extremely slow and dockers failed to start, the application couldn't be deployed on the cluster.

PS: There was a confusion in representing the business logic mentioned. Hence the output are not accurate.

